module.exports = (app) => {
  const cata = require("../controllers/cata.controller.js");
  var router = require("express").Router();
  const appToken = require("../config/apptoken.config.js");

  router.get("/data/all", cata.dataAll);
  router.get("/data/:id", cata.data);
  router.post("/data/add", cata.addData);
  router.post("/data/edit", cata.editData);
  router.post("/data/delete", cata.deleteData);
  router.post("/data/mutiprocess", cata.mutiProcess);

  app.use("/api/v1/catalog", router);
};
