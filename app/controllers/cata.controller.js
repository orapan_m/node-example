var catalog = require("../models/cata.model.js");
var log = require("../config/log.config.js");

function keepLog(req, result) {
  const log = {
    app: req.headers.application,
    url: req.originalUrl,
    method: req.method,
    body: req.body,
    date: Date.now(),
    result: result,
  };
  console.log(log);
}
exports.dataAll = (req, res) => {
  catalog.getDataAll().then((data) => {
    log.keep && keepLog(req, data);
    res.status(data.code).json(data);
  });
};

exports.data = (req, res) => {
  if (req.params.id) {
    const id = req.params.id;
    catalog.getData(id).then((data) => {
      log.keep && keepLog(req, data);
      res.status(data.code).json(data);
    });
  }
};

exports.addData = (req, res) => {
  const params = req.body;
  catalog.addData(params).then((data) => {
    log.keep && keepLog(req, data);
    res.status(data.code).json(data);
  });
};

exports.editData = (req, res) => {
  const params = req.body;
  catalog.editData(params).then((data) => {
    log.keep && keepLog(req, data);
    res.status(data.code).json(data);
  });
};

exports.deleteData = (req, res) => {
  const params = req.body;
  catalog.deleteData(params).then((data) => {
    log.keep && keepLog(req, data);
    res.status(data.code).json(data);
  });
};

exports.mutiProcess = (req, res) => {
  const params = req.body;
  catalog.mutiProcess(params).then((data) => {
    log.keep && keepLog(req, data);
    res.status(data.code).json(data);
  });
};
