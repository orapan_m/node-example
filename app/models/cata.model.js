module.exports = {
  getDataAll,
  getData,
  addData,
  editData,
  deleteData,
  mutiProcess,
};
const dbConfig = require("../config/db.config.js");
const sql = require("mssql");
const qr = require("./cata.query.js");
const db = dbConfig.test;

async function getDataAll() {
  let pool = await sql.connect(db);
  try {
    const request = new sql.Request();
    const result = await request.query(qr.dataAll());
    return { result: true, code: 200, data: result.recordset, msg: "success" };
  } catch (error) {
    pool.close();
    return { result: false, code: 400, data: [], msg: error };
  } finally {
    pool.close();
  }
}

async function getData(id) {
  let pool = await sql.connect(db);
  try {
    const request = new sql.Request();
    const result = await request.query(qr.data(id));
    return { result: true, code: 200, data: result.recordset, msg: "success" };
  } catch (error) {
    pool.close();
    return { result: false, code: 400, data: [], msg: error };
  } finally {
    pool.close();
  }
}

async function addData(params) {
  let pool = await sql.connect(db);
  try {
    const request = new sql.Request();
    const result = await request.query(qr.addData(params));
    return {
      result: true,
      code: 200,
      data: result.rowsAffected[0],
      msg: "Inserted Successfuly",
    };
  } catch (error) {
    pool.close();
    return { result: false, code: 400, data: 0, msg: error };
  } finally {
    pool.close();
  }
}

async function editData(params) {
  let pool = await sql.connect(db);
  try {
    const request = new sql.Request();
    const result = await request.query(qr.editData(params));
    return {
      result: true,
      code: 200,
      data: result.rowsAffected[0],
      msg: "Updated Successfuly",
    };
  } catch (error) {
    pool.close();
    return { result: false, code: 400, data: 0, msg: error };
  } finally {
    pool.close();
  }
}

async function deleteData(params) {
  let pool = await sql.connect(db);
  try {
    const request = new sql.Request();
    const result = await request.query(qr.deleteData(params));
    return {
      result: true,
      code: 200,
      data: result.rowsAffected[0],
      msg: "Deleted Successfuly",
    };
  } catch (error) {
    pool.close();
    return { result: false, code: 400, data: 0, msg: error };
  } finally {
    pool.close();
  }
}

async function mutiProcess(params) {
  let pool = await sql.connect(db);
  const transaction = new sql.Transaction(pool);
  try {
    await transaction.begin();
    const request1 = new sql.Request(transaction);
    const request2 = new sql.Request(transaction);
    const result1 = await request1.query(qr.addData(params));
    const result2 = await request2.query(qr.addData2(params));
    await transaction.commit();
    return {
      result: true,
      code: 200,
      data: [result1.rowsAffected[0], result2.rowsAffected[0]],
      msg: "Processed Successfuly",
    };
  } catch (error) {
    await transaction.rollback();
    pool.close();
    return { result: false, code: 400, data: 0, msg: error };
  } finally {
    pool.close();
  }
}
