function dataAll() {
  const query = "SELECT * FROM Table_1";
  return query;
}

function data(id) {
  const query = `SELECT * FROM Table_1 where id= ${id}`;
  return query;
}

function addData(params) {
  const query = `INSERT INTO Table_1(id,name) 
                VALUES(${params.id} , '${params.name}')`;

  return query;
}

function addData2(params) {
  const query = `INSERT INTO Table_2x(id,name) 
                VALUES(${params.id} , '${params.name}')`;
  return query;
}

function editData(params) {
  const query = `UPDATE Table_1
                SET name = '${params.name}'
                WHERE id = ${params.id}`;
  return query;
}

function deleteData(params) {
  const query = `DELETE Table_1
                WHERE id= ${params.id}`;
  return query;
}

module.exports = {
  dataAll,
  data,
  addData,
  editData,
  deleteData,
  addData2,
};
