const express = require("express");
const cors = require("cors");
const app = express();

const appToken = require("./app/config/apptoken.config.js");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use((req, res, next) => {
  const token = req.headers.application;
  if (typeof appToken[token] !== "undefined") next();
  else res.status(403).send({ statuscode: 403, msg: "Forbiden" }).end();
});

app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

require("./app/routes/cata.route")(app);

app.listen(9001);
